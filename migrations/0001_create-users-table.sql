CREATE TABLE "users" (
    id BIGSERIAL PRIMARY KEY,
    username varchar NOT NULL,
    password_hash varchar NOT NULL,
    email varchar NOT NULL,
    first_name varchar NOT NULL,
    last_name varchar NOT NULL,
    active boolean DEFAULT TRUE,
    country varchar,
    balance INT
);

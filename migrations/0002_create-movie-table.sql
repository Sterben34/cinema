CREATE TABLE "movie" (
    id BIGSERIAL PRIMARY KEY,
	title TEXT NOT NULL,
	description TEXT NOT NULL,
	cover TEXT,
	movie TEXT,
	rating SMALLINT,
	year SMALLINT,
	genre VARCHAR,
	"worldGross" INT,
	price INT
);

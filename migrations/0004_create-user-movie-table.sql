CREATE TABLE "user_movie" (
    id BIGSERIAL PRIMARY KEY,
	user_id BIGINT REFERENCES users(id),
	movie_id BIGINT REFERENCES movie(id)
);

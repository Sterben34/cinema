CREATE TABLE "comments" (
    id BIGSERIAL PRIMARY KEY,
	author BIGINT REFERENCES users(id),
	text text NOT NULL,
	movie_id BIGINT REFERENCES movie(id)
);

CREATE TABLE "user_token" (
    id BIGSERIAL PRIMARY KEY,
	user_id BIGINT REFERENCES users(id),
	token VARCHAR,
	expired_at DATE
);

import { ModuleContract } from '../../contract/module.contract';
import { method, Route } from '../../contract/route.contract';
import { AddCommentToMovieCommand } from './commands/add-comment-to-movie.command';
import { BuyMovieCommand } from './commands/buy-movie.command';
import { GetCommentsByMovieCommand } from './commands/get-comments-by-movie.command';
import { GetMovieListCommand } from './commands/get-movie-list.command';
import { GetMovieCommand } from './commands/get-movie.command';
import { GetMyMoviesCommand } from './commands/get-my-movies.command';

export class MovieModule implements ModuleContract {
	basePath = '/movie';

	routes: Route[] = [
		{
			method: method.POST,
			path: '',
			command: GetMovieListCommand,
		},
		{
			method: method.GET,
			path: '/:id',
			command: GetMovieCommand,
		},
		{
			method: method.POST,
			path: '/buy',
			command: BuyMovieCommand,
		},
		{
			method: method.GET,
			path: '/my-movies',
			command: GetMyMoviesCommand,
		},
		{
			method: method.GET,
			path: '/comments/:movie_id',
			command: GetCommentsByMovieCommand,
		},
		{
			method: method.POST,
			path: '/comments/:movie_id',
			command: AddCommentToMovieCommand,
		},
	];
}

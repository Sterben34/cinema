import { BaseRepositoryContract } from '../../../contract/base-repository.contract';
import { CommentModel } from '../models/comment.model';

export class CommentsRepository extends BaseRepositoryContract<CommentModel> {
	constructor() {
		super();
		this.tableName = 'comments';
	}
}

export const commentsRepository = new CommentsRepository();

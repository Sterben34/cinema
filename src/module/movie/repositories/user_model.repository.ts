import { BaseRepositoryContract } from '../../../contract/base-repository.contract';
import { UserMovieModel } from '../models/user_movie.model';

export class UserMovieRepository extends BaseRepositoryContract<UserMovieModel> {
	constructor() {
		super();
		this.tableName = 'user_movie';
	}
}

export const userMovieRepository = new UserMovieRepository();

import { UserModel } from './../../user/model/user.model';
import { userStorage } from './../../user/storage/user.storage';
import { BaseRepositoryContract } from '../../../contract/base-repository.contract';
import { MovieModel } from '../models/movie.model';

export class MovieRepository extends BaseRepositoryContract<MovieModel> {
	constructor() {
		super();
		this.tableName = 'movie';
	}

	async getMy(userId: number): Promise<MovieModel[]> {
		const sql = `SELECT * FROM ${this.tableName}
					JOIN user_movie ON ${this.tableName}.id = user_movie.movie_id
					WHERE user_id = ${userId}`;
		return (await this.connection.fetchRows(sql)).map((i) => <MovieModel>i);
	}
	async getCountOfRecord(
		search: string,
		generes: string[],
		years: number[],
		my: boolean,
	): Promise<number> {
		console.log(1);

		const sql = `SELECT count(*) FROM ${
			this.tableName
		} LEFT JOIN user_movie ON user_movie.movie_id = movie.id LEFT JOIN users ON user_movie.user_id = users.id WHERE (title LIKE '%${search}%' OR description LIKE '%${search}%') ${
			generes.length > 0 ? `AND genre IN ('${generes.join("','")}')` : ''
		} ${years.length > 0 ? `AND year IN ('${years.join("','")}')` : ''} ${
			my ? ' AND users.id = ' + (userStorage.get() as UserModel).id : ''
		}`;
		console.log(2);
		console.log(sql);

		return (await this.connection.fetchScalar(sql)) as number;
	}
	async getAllWithPagination(
		limit: 'ALL' | number = 'ALL',
		offset = 0,
		search = '',
		generes: string[],
		years: number[],
		my: boolean,
	): Promise<Array<MovieModel>> {
		let sql = `SELECT movie.id,
		title,
		description,
		cover,
		movie,
		rating,
		year,
		genre,
		"worldGross",
		price FROM ${this.tableName} LEFT JOIN user_movie ON user_movie.movie_id = movie.id LEFT JOIN users ON user_movie.user_id = users.id `;

		sql += ` WHERE (title LIKE '%${search}%' OR description LIKE '%${search}%') ${
			generes.length > 0 ? `AND genre IN ('${generes.join("','")}')` : ''
		} ${years.length > 0 ? `AND year IN ('${years.join("','")}')` : ''} ${
			my ? ' AND users.id = ' + (userStorage.get() as UserModel).id : ''
		} LIMIT ${limit} OFFSET ${offset}`;

		const source = await this.connection.fetchRows(sql, []);
		return source.map((source) => <MovieModel>source);
	}
}

export const movieRepository = new MovieRepository();

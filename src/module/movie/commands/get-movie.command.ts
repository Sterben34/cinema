import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { userStorage } from '../../user/storage/user.storage';
import { movieRepository } from '../repositories/movie.repository';
import { userMovieRepository } from '../repositories/user_model.repository';

export class GetMovieCommand extends CommandContract {
	constructor() {
		super();
	}
	async run(req: Request): Promise<object> {
		const movieId = Number(req.params.id);

		const movie = await movieRepository.findOneById(movieId);
		if (!movie) {
			return { message: 'No movie with such id.' };
		}
		const user = userStorage.get();
		const movie_id = movie.id;

		if (user) {
			const a = await userMovieRepository.findAll({ user_id: user.id, movie_id });

			return {
				...movie,
				isBougth: a.length > 0 ? true : false,
			};
		}
		return movie;
	}
}

import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { UserModel } from '../../user/model/user.model';
import { userStorage } from '../../user/storage/user.storage';
import { CommentModel } from '../models/comment.model';
import { commentsRepository } from '../repositories/comments.repository';

export class AddCommentToMovieCommand extends CommandContract {
	constructor() {
		super();
		this.onlyAuthorized = true;
	}
	async run(req: Request): Promise<CommentModel> {
		const user = <UserModel>userStorage.get();

		return commentsRepository.insert(<CommentModel>{
			author: user.id,
			text: req.body.text,
			movie_id: Number(req.params.movie_id),
		});
	}
}

import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { UserModel } from '../../user/model/user.model';
import { userRepository } from '../../user/repository/user.repository';
import { userStorage } from '../../user/storage/user.storage';
import { MovieModel } from '../models/movie.model';
import { UserMovieModel } from '../models/user_movie.model';
import { movieRepository } from '../repositories/movie.repository';
import { userMovieRepository } from '../repositories/user_model.repository';

export class BuyMovieCommand extends CommandContract {
	constructor() {
		super();
		this.onlyAuthorized = true;
	}
	async run(req: Request): Promise<object> {
		const user = <UserModel>userStorage.get();
		const movieId = req.body.movieId;
		const movie = await movieRepository.findOneById(movieId);
		if (!movie) {
			return { message: 'No movie with such id.' };
		}
		// if (user.balance < movie.price) {
		// 	return { message: 'Not enough balance.' };
		// }
		// user.balance -= movie.price;
		userMovieRepository.insert(<UserMovieModel>{
			user_id: user.id,
			movie_id: movie.id,
		});
		userRepository.update(user);

		return { message: 'Was successfully bought.' };
	}
}

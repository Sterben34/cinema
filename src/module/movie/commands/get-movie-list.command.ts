import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { movieRepository } from '../repositories/movie.repository';

export class GetMovieListCommand extends CommandContract {
	async run(req: Request): Promise<any> {
		const search = String(req.body.searchStr);
		const generes: string[] = req.body.generes ?? [];
		const my = Boolean(req.body.my);
		const years: number[] = req.body.years ?? [];
		const count_of_records = await movieRepository.getCountOfRecord(search, generes, years, my);

		const page = req.body.page ? Number(req.body.page) : 1;
		const limit = req.body.limit ? Number(req.body.limit) : 50;
		const offset = (page - 1) * limit;

		const movieList = await movieRepository.getAllWithPagination(
			limit,
			offset,
			search,
			generes,
			years,
			my,
		);

		const count_of_pages = Math.ceil(count_of_records / limit);

		return {
			count_of_pages: count_of_pages,
			current_page: page,
			items: movieList,
		};
	}
}

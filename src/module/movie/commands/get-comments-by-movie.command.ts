import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { commentsRepository } from '../repositories/comments.repository';

export class GetCommentsByMovieCommand extends CommandContract {
	async run(req: Request): Promise<any> {
		const movie_id = Number(req.params.movie_id);
		const comments = commentsRepository.findAll({ movie_id: movie_id });
		return {
			items: comments,
		};
	}
}

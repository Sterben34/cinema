import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { userStorage } from '../../user/storage/user.storage';
import { movieRepository } from '../repositories/movie.repository';

export class GetMyMoviesCommand extends CommandContract {
	constructor() {
		super();
		this.onlyAuthorized = true;
	}
	async run(req: Request): Promise<any> {
		return await movieRepository.getMy(<number>(<unknown>userStorage.get()));
	}
}

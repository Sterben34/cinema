export interface UserMovieModel {
	id: number;
	user_id: number;
	movie_id: number;
}

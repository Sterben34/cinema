export interface CommentModel {
	id: number;
	text: string;
	author: number;
	movie_id: number;
}

export interface MovieModel {
	id: number;
	title: string;
	description: string;
	cover: string;
	movie: string;
	rating: number;
	year: number;
	genre: string;
	worldGross: number;
	price: number;
}

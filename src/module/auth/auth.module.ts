import { ModuleContract } from '../../contract/module.contract';
import { method, Route } from '../../contract/route.contract';
import { SignUpCommand } from './command/sign-up.command';
import { SignInCommand } from './command/sign-in.command';
import { ResetPasswordCommand } from './command/reset-password.command';

export class AuthModule implements ModuleContract {
	basePath = '/auth';

	routes: Route[] = [
		{
			method: method.POST,
			path: '/sign-up',
			command: SignUpCommand,
		},
		{
			method: method.POST,
			path: '/sign-in',
			command: SignInCommand,
		},
		{
			method: method.POST,
			path: '/reset-password',
			command: ResetPasswordCommand,
		},
	];
}

import { CommandContract } from '../../../contract/command.contract';
import { Request } from 'express';
import { SignUpDto } from './dto/sign-up.dto';
import { SignUpResponseDto } from './dto/sign-up-response.dto';
import {
	signUpValidationHelper,
	SignUpValidationHelper,
} from '../helpers/sign-up-validation.helper';
import { UserCreationHelper, userCreationHelper } from '../../user/helpers/creating-user.helper';
import { AddUserDto } from '../../user/command/dto/add-user.dto';

export class SignUpCommand extends CommandContract {
	private readonly signUpValidationHelper: SignUpValidationHelper;
	private readonly userCreationHelper: UserCreationHelper;

	constructor() {
		super();
		this.endpointDescription = "Sign up user with role 'parent'";
		this.unprocessableEntityComment =
			'User with email already exists.\nPasswords must match.\nUser with username already exists.';
		this.signUpValidationHelper = signUpValidationHelper;
		this.userCreationHelper = userCreationHelper;
	}

	async run(req: Request): Promise<any> {
		const signUpRequestDto = <SignUpDto>{ ...req.body };

		await this.signUpValidationHelper.validate(signUpRequestDto);

		const createdUser = await this.userCreationHelper.createUser(<AddUserDto>signUpRequestDto);

		return <SignUpResponseDto>{
			user_id: createdUser.id,
		};
	}
}

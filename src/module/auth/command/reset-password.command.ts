import { CommandContract } from '../../../contract/command.contract';
import { Request } from 'express';
import { UserStorage, userStorage } from '../../user/storage/user.storage';
import { UserModel } from '../../user/model/user.model';
import { ResetPasswordDto } from './dto/reset-password.dto';
import { UnprocessableEntityError } from '../../../error/unprocessable-entity.error';
import { userRepository } from '../../user/repository/user.repository';
import { SecureService } from '../../utils/service/secure.service';

export class ResetPasswordCommand extends CommandContract {
	private userStorage: UserStorage;

	constructor() {
		super();
		this.userStorage = userStorage;
	}

	async run(req: Request): Promise<UserModel> {
		const requestDto = <ResetPasswordDto>{ ...req.body };
		const currentUser = <UserModel>this.userStorage.get();

		if (requestDto.password !== requestDto.password_confirm) {
			throw new UnprocessableEntityError('Passwords must match.');
		}
		currentUser.password_hash = await SecureService.hash(requestDto.password);

		return await userRepository.update(currentUser);
	}
}

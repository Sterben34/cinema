export interface SignUpDto {
	email: string;
	username: string;
	password: string;
	password_confirm: string;
	first_name: string;
	last_name: string;
	country: string;
}

export interface SignInDto {
	email?: string;
	username?: string;
	password: string;
}

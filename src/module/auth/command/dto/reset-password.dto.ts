export interface ResetPasswordDto {
	password: string;
	password_confirm: string;
}

export interface ResetPasswordResponseDto {
	message: string;
}

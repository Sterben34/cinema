import { CommandContract } from '../../../contract/command.contract';
import { Request } from 'express';
import { GenerateUserTokenService } from '../../user/service/generate-user-token.service';
import { SignInResponseDto } from './dto/sign-in-response.dto';
import { signInHelper, SignInHelper } from '../helpers/sign-in.helper';
import { SignInDto } from './dto/sign-in.dto';
import { SecureService } from '../../utils/service/secure.service';
import { UnprocessableEntityError } from '../../../error/unprocessable-entity.error';

export class SignInCommand extends CommandContract {
	private readonly generateUserTokenService: GenerateUserTokenService;
	private readonly signInHelper: SignInHelper;

	constructor() {
		super();
		this.endpointDescription = 'Sign in user';
		this.unprocessableEntityComment = 'Incorrect password.\nIncorrect email.\nIncorrect username.';
		this.signInHelper = signInHelper;
		this.generateUserTokenService = new GenerateUserTokenService();
		this.onlyAuthorized = false;
	}

	async run(req: Request): Promise<any> {
		const requestDto = <SignInDto>{ ...req.body };

		const existingUser = await this.signInHelper.getExistingUser(requestDto);
		const isValid = await SecureService.compareStringWithHash(
			requestDto.password,
			existingUser.password_hash,
		);
		if (!isValid) {
			throw new UnprocessableEntityError(`Incorrect password.`);
		}

		const token = await this.generateUserTokenService.generate(existingUser);

		return <SignInResponseDto>{
			token,
		};
	}
}

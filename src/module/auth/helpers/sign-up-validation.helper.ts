import { UnprocessableEntityError } from '../../../error/unprocessable-entity.error';
import { UserRepository, userRepository } from '../../user/repository/user.repository';
import { SignUpDto } from '../command/dto/sign-up.dto';

export class SignUpValidationHelper {
	private readonly userRepository: UserRepository;

	constructor() {
		this.userRepository = userRepository;
	}

	async validate(signUpDto: SignUpDto): Promise<void> {
		if (signUpDto.password !== signUpDto.password_confirm) {
			throw new UnprocessableEntityError('Passwords must match.');
		}

		let existingUser = await this.userRepository.findOneByUsername(signUpDto.username);
		if (existingUser) {
			throw new UnprocessableEntityError(
				`User with username "${signUpDto.username}" already exists.`,
			);
		}

		existingUser = await this.userRepository.findOneByEmail(signUpDto.email);
		if (existingUser) {
			throw new UnprocessableEntityError(`User with email "${signUpDto.email}" already exists.`);
		}
	}
}
export const signUpValidationHelper = new SignUpValidationHelper();

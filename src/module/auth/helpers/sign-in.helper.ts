import { UnprocessableEntityError } from '../../../error/unprocessable-entity.error';
import { UserRepository, userRepository } from '../../user/repository/user.repository';
import { auth_config, AUTH_TYPES } from '../../../config/auth.config';
import { UserModel } from '../../user/model/user.model';
import { SignInDto } from '../command/dto/sign-in.dto';

export class SignInHelper {
	private readonly userRepository: UserRepository;

	constructor() {
		this.userRepository = userRepository;
	}

	async getExistingUser(requestDto: SignInDto): Promise<UserModel> {
		let existingUser: UserModel | null;
		let wrongField = 'username';

		switch (auth_config.auth_type) {
			case AUTH_TYPES.email:
				existingUser = await this.userRepository.findOneByEmail(<string>requestDto.email);
				wrongField = 'email';
				break;
			case AUTH_TYPES.username:
				existingUser = await this.userRepository.findOneByUsername(<string>requestDto.username);
				break;
			default:
				// Either email or username
				wrongField = 'email or username';
				existingUser = await this.userRepository.findOneByUsername(<string>requestDto.username);
				if (!existingUser) {
					existingUser = await this.userRepository.findOneByEmail(<string>requestDto.email);
				}
				break;
		}

		if (!existingUser) {
			throw new UnprocessableEntityError(`Incorrect ${wrongField}.`);
		}

		return existingUser;
	}
}
export const signInHelper = new SignInHelper();

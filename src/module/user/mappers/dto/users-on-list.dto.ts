export interface UserOnListDto {
	id: number;
	username: string;
	email: string;
	first_name: string;
	last_name: string;
	active: boolean;
	country: string;
	balance: number;
}

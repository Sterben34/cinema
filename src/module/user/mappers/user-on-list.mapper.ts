import { UserModel } from '../model/user.model';
import { UserOnListDto } from './dto/users-on-list.dto';

export class UserOnListMapper {
	map(userModel: UserModel): UserOnListDto {
		return {
			id: userModel.id,
			username: userModel.username,
			email: userModel.email,
			first_name: userModel.first_name,
			last_name: userModel.last_name,
			active: userModel.active,
			country: userModel.country,
			balance: userModel.balance,
		};
	}
}

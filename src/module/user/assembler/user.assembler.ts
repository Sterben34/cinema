import { NotFoundError } from '../../../error/not-found.error';
import { UserModel } from '../model/user.model';
import { UserRepository, userRepository } from '../repository/user.repository';
import { GetUserResponseDto } from '../command/dto/user-response.dto';

export class UserAssembler {
	private readonly userRepository: UserRepository;

	constructor() {
		this.userRepository = userRepository;
	}

	async assemble(userId: number): Promise<GetUserResponseDto> {
		const user = await this.userRepository.findOneById(userId);
		if (!user) {
			throw new NotFoundError('User not found.');
		}

		return this.modelToDto(user);
	}

	modelToDto(userModel: UserModel): GetUserResponseDto {
		return <GetUserResponseDto>{
			id: userModel.id,
			username: userModel.username,
			email: userModel.email,
			first_name: userModel.first_name,
			last_name: userModel.last_name,
			active: userModel.active,
			country: userModel.country,
			balance: userModel.balance,
		};
	}
}

import { BaseRepositoryContract } from '../../../contract/base-repository.contract';
import { UserModel } from '../model/user.model';

export class UserRepository extends BaseRepositoryContract<UserModel> {
	constructor() {
		super();
		this.tableName = 'users';
	}
	async setActiveState(userId: number, state: boolean): Promise<UserModel> {
		const user = await this.connection.fetchRow(
			`UPDATE users SET active = $1 WHERE id = $2 RETURNING *`,
			[state, userId],
		);
		return <UserModel>user;
	}

	async findOneByUsername(username: string): Promise<UserModel | null> {
		const user = await this.connection.fetchRow(`SELECT * FROM users WHERE username = $1`, [
			username,
		]);
		if (!user) {
			return null;
		}
		return <UserModel>user;
	}

	async findOneByEmail(email: string): Promise<UserModel | null> {
		const user = await this.connection.fetchRow(`SELECT * FROM users WHERE email = $1`, [email]);
		if (!user) {
			return null;
		}
		return <UserModel>user;
	}
}

export const userRepository = new UserRepository();

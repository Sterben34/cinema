import { BaseRepositoryContract } from '../../../contract/base-repository.contract';
import { UserTokenModel } from '../model/user-token.model';

export class UserTokenRepository extends BaseRepositoryContract<UserTokenModel> {
	constructor() {
		super();
		this.tableName = 'user_token';
	}

	async findOneNotExpiredByToken(token: string): Promise<UserTokenModel | null> {
		const sql = `SELECT * FROM "user_token" WHERE token = $1 AND expired_at >= NOW()`;
		const source = await this.connection.fetchRow(sql, [token]);
		if (!source) {
			return null;
		}

		return <UserTokenModel>source;
	}
	async expireAllExceptCurrentToken(userId: number, currentTokenId: number): Promise<void> {
		this.connection.fetchRow(
			`UPDATE "${this.tableName}" SET expired_at = NOW() WHERE user_id = $1 AND id != $2`,
			[userId, currentTokenId],
		);
	}
}

export const userTokenRepository = new UserTokenRepository();

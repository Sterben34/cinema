import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { NotFoundError } from '../../../error/not-found.error';
import { SecureService } from '../../utils/service/secure.service';
import { UserAssembler } from '../assembler/user.assembler';
import { UserModel } from '../model/user.model';
import { UserRepository, userRepository } from '../repository/user.repository';
import { EditUserDto, EditUserResponseDto } from './dto/edit-user.dto';

export class EditUserCommand extends CommandContract {
	private readonly userRepository: UserRepository;
	private readonly userAssembler: UserAssembler;

	constructor() {
		super();
		this.onlyAuthorized = false;
		this.endpointDescription = 'Editing user by id';
		this.notFoundComment = 'User with such id not found.';
		this.userRepository = userRepository;
		this.userAssembler = new UserAssembler();
	}

	async run(req: Request): Promise<EditUserResponseDto> {
		const editUserDto = <EditUserDto>{ ...req.body, id: Number(req.params.id) };
		const user = await this.userRepository.findOneById(editUserDto.id);
		if (!user) {
			throw new NotFoundError('User not found.');
		}

		const userModel = <UserModel>{
			id: editUserDto.id,
			username: editUserDto.username ?? user.username,
			password_hash: editUserDto.password
				? await SecureService.hash(editUserDto.password)
				: user.password_hash,
			email: editUserDto.email ?? user.email,
			first_name: editUserDto.first_name ?? user.first_name,
			last_name: editUserDto.last_name ?? user.last_name,
		};

		const updatedUser = await this.userRepository.update(userModel);

		const userDto = this.userAssembler.modelToDto(updatedUser);
		return <EditUserResponseDto>userDto;
	}
}

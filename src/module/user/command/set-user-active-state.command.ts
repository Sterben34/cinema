import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { NotFoundError } from '../../../error/not-found.error';
import { UserAssembler } from '../assembler/user.assembler';
import { UserRepository, userRepository } from '../repository/user.repository';
import {
	SetUserActiveStateDto,
	SetUserActiveStateResponseDto,
} from './dto/set-user-active-state.dto';

export class SetUserActiveStateCommand extends CommandContract {
	private readonly userRepository: UserRepository;
	private readonly userAssembler: UserAssembler;

	constructor() {
		super();
		this.onlyAuthorized = false;
		this.endpointDescription = 'Activate or deactivate user.';
		this.notFoundComment = 'User with such id not found.';
		this.userRepository = userRepository;
		this.userAssembler = new UserAssembler();
	}

	async run(req: Request): Promise<SetUserActiveStateResponseDto> {
		const setUserActiveStateDto = <SetUserActiveStateDto>{ ...req.body, id: Number(req.params.id) };

		const user = await this.userRepository.findOneById(setUserActiveStateDto.id);
		if (!user) {
			throw new NotFoundError('User not found.');
		}

		const updatedUser = await this.userRepository.setActiveState(
			setUserActiveStateDto.id,
			setUserActiveStateDto.active,
		);

		const userDto = this.userAssembler.modelToDto(updatedUser);
		return <SetUserActiveStateResponseDto>userDto;
	}
}

import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { UserModel } from '../model/user.model';
import { userStorage } from '../storage/user.storage';

export class GetMeCommand extends CommandContract {
	constructor() {
		super();
		this.onlyAuthorized = true;
	}
	async run(req: Request): Promise<UserModel> {
		return <UserModel>userStorage.get();
	}
}

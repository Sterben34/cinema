import { GetUserResponseDto } from './user-response.dto';

export interface DeleteUserResponseDto extends GetUserResponseDto {}

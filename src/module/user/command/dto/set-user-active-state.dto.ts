import { GetUserResponseDto } from './user-response.dto';

export interface SetUserActiveStateDto {
	id: number;
	active: boolean;
}
export interface SetUserActiveStateResponseDto extends GetUserResponseDto {}

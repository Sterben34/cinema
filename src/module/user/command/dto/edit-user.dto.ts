import { GetUserResponseDto } from './user-response.dto';

export interface EditUserDto {
	id: number;
	email: string;
	username: string;
	password: string;
	first_name: string;
	last_name: string;
	city_id: number;
}
export interface EditUserResponseDto extends GetUserResponseDto {}

import { GetUserResponseDto } from './user-response.dto';

export interface AddUserDto {
	email: string;
	username: string;
	password: string;
	first_name: string;
	last_name: string;
	country: string;
}
export interface AddUserResponseDto extends GetUserResponseDto {}

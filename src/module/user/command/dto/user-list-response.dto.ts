import { UserOnListDto } from '../../mappers/dto/users-on-list.dto';

export interface GetUsersListResponseDto {
	items: UserOnListDto[];
}

import { CommandContract } from '../../../contract/command.contract';
import { Request } from 'express';
import { GetUsersListResponseDto } from './dto/user-list-response.dto';
import { userRepository, UserRepository } from '../repository/user.repository';
import { UserOnListMapper } from '../mappers/user-on-list.mapper';

export class GetUsersListCommand extends CommandContract {
	private readonly userRepository: UserRepository;
	private readonly userOnListMapper: UserOnListMapper;

	constructor() {
		super();
		this.onlyAuthorized = false;
		this.endpointDescription = 'Get user list.';
		this.userRepository = userRepository;
		this.userOnListMapper = new UserOnListMapper();
	}

	async run(req: Request): Promise<GetUsersListResponseDto> {
		const users = await this.userRepository.findAll();

		return <GetUsersListResponseDto>{
			items: users.map((user) => {
				return this.userOnListMapper.map(user);
			}),
		};
	}
}

import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { UserAssembler } from '../assembler/user.assembler';
import { UserCreationHelper, userCreationHelper } from '../helpers/creating-user.helper';
import { AddUserDto, AddUserResponseDto } from './dto/add-user.dto';

export class AddUserCommand extends CommandContract {
	private readonly userAssembler: UserAssembler;
	private readonly userCreationHelper: UserCreationHelper;

	constructor() {
		super();
		this.onlyAuthorized = false;
		this.endpointDescription = 'Add user.';
		this.userAssembler = new UserAssembler();
		this.userCreationHelper = userCreationHelper;
	}
	async run(req: Request): Promise<AddUserResponseDto> {
		const addUserDto = <AddUserDto>{ ...req.body };

		const createdUser = await this.userCreationHelper.createUser(addUserDto);

		const userDto = this.userAssembler.modelToDto(createdUser);

		return <AddUserResponseDto>userDto;
	}
}

import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { UserAssembler } from '../assembler/user.assembler';
import { UserModel } from '../model/user.model';
import { UserRepository, userRepository } from '../repository/user.repository';
import { DeleteUserResponseDto } from './dto/delete-user.dto';

export class DeleteUserCommand extends CommandContract {
	private readonly userRepository: UserRepository;
	private readonly userAssembler: UserAssembler;

	constructor() {
		super();
		this.onlyAuthorized = false;
		this.endpointDescription = 'Delete user by id.';
		this.notFoundComment = 'User with such id not found.';
		this.userRepository = userRepository;
		this.userAssembler = new UserAssembler();
	}

	async run(req: Request): Promise<DeleteUserResponseDto> {
		const userId = Number(req.params.id);

		const deletedUser = await this.userRepository.delete(userId);

		return <DeleteUserResponseDto>this.userAssembler.modelToDto(<UserModel>deletedUser);
	}
}

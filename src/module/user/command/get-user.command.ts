import { Request } from 'express';
import { CommandContract } from '../../../contract/command.contract';
import { GetUserResponseDto } from './dto/user-response.dto';
import { UserAssembler } from '../assembler/user.assembler';

export class GetUserCommand extends CommandContract {
	private userAssembler: UserAssembler;

	constructor() {
		super();
		this.onlyAuthorized = false;
		this.endpointDescription = 'Get user by id';
		this.notFoundComment = 'User with such id not found.';
		this.userAssembler = new UserAssembler();
	}

	async run(req: Request): Promise<GetUserResponseDto> {
		const userId = Number(req.params.id);
		const userDto = await this.userAssembler.assemble(userId);

		return userDto;
	}
}

export interface UserTokenModel {
	id: number;
	user_id: number;
	token: string;
	expired_at: string;
	created_at: string;
}

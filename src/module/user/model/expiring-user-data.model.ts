export interface ExpiringUserDataModel {
	id: number;
	data: string;
	user_id: number;
	expired_at: string;
	created_at: string;
}

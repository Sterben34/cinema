import { ForbiddenError } from '../../../error/forbidden.error';
import { UnprocessableEntityError } from '../../../error/unprocessable-entity.error';

export class UserRole {
	public static PARENT = 'Parent';
	public static STUDENT = 'Student';

	public readonly role: string;

	constructor(role: string) {
		this.role = role;
	}

	getId(): number {
		switch (this.role) {
			case UserRole.PARENT:
				return 1;
			case UserRole.STUDENT:
				return 2;
			default:
				throw new UnprocessableEntityError('No such role');
		}
	}

	isParent(): boolean {
		return this.role === UserRole.PARENT;
	}
	isStudent(): boolean {
		return this.role === UserRole.STUDENT;
	}
}

export class AccessValidator {
	static validateAccess(roleId: number, availableRolesList: string[]): void {
		const roleName = AccessValidator.getRoleById(roleId);
		if (!availableRolesList.includes(roleName)) {
			throw new ForbiddenError(`This command is not available for ${roleName}.`);
		}
	}

	static getRoleById(id: number): string {
		switch (id) {
			case 1:
				return UserRole.PARENT;
			case 2:
				return UserRole.STUDENT;
			default:
				throw new UnprocessableEntityError('No such role');
		}
	}
}

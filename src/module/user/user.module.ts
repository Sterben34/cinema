import { ModuleContract } from '../../contract/module.contract';
import { method, Route } from '../../contract/route.contract';
import { AddUserCommand } from './command/add-user.command';
import { DeleteUserCommand } from './command/delete-user.command';
import { EditUserCommand } from './command/edit-user.command';
import { GetMeCommand } from './command/get-me.command';
import { GetUserCommand } from './command/get-user.command';
import { GetUsersListCommand } from './command/get-users-list.command';
import { SetUserActiveStateCommand } from './command/set-user-active-state.command';

export class UserModule implements ModuleContract {
	basePath = '/user';

	routes: Route[] = [
		{
			method: method.GET,
			path: '',
			command: GetUsersListCommand,
		},
		{
			method: method.GET,
			path: '/:id',
			command: GetUserCommand,
		},
		{
			method: method.POST,
			path: '',
			command: AddUserCommand,
		},
		{
			method: method.PUT,
			path: '/:id',
			command: EditUserCommand,
		},
		{
			method: method.DELETE,
			path: '/:id',
			command: DeleteUserCommand,
		},
		{
			method: method.POST,
			path: '/:id/set-active-state',
			command: SetUserActiveStateCommand,
		},
		{
			method: method.GET,
			path: '/me',
			command: GetMeCommand,
		},
	];
}

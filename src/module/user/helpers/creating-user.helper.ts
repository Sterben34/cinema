import { UserModel } from '../model/user.model';
import { UserRepository, userRepository } from '../repository/user.repository';

import { AddUserDto } from '../command/dto/add-user.dto';
import { SecureService } from '../../utils/service/secure.service';

export class UserCreationHelper {
	private readonly userRepository: UserRepository;

	constructor() {
		this.userRepository = userRepository;
	}

	async createUser(addUserDto: AddUserDto): Promise<UserModel> {
		const userModel = <UserModel>{
			username: addUserDto.username,
			password_hash: await SecureService.hash(addUserDto.password),
			email: addUserDto.username,
			first_name: addUserDto.username,
			last_name: addUserDto.username,
			active: true,
			country: addUserDto.username,
			balance: 1000,
		};

		return this.userRepository.insert(userModel);
	}
}
export const userCreationHelper = new UserCreationHelper();

import { secure } from '../../../config/secure';
import * as bcrypt from 'bcrypt';

export class SecureService {
	static async hash(text: string): Promise<string> {
		return await bcrypt.hash(text, secure.saltRounds);
	}

	static async compareStringWithHash(string: string, actualHash: string): Promise<boolean> {
		return await bcrypt.compare(string, actualHash);
	}
}
export const secureService = new SecureService();

export class UnprocessableEntityError extends Error {
	code = 422;
}

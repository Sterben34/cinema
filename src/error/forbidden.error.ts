export class ForbiddenError extends Error {
	code = 403;
}

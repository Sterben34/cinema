import 'dotenv/config';
import { App } from './app';

async function bootstrap(): Promise<any> {
	const app = new App();
	await app.init();
	return { app };
}

export const boot = bootstrap();

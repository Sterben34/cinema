import { connection, Connection } from '../db/connection';
import { ServerErrorError } from '../error/server-error.error';

export class BaseRepositoryContract<Type> {
	// Active connection to DB
	protected readonly connection: Connection;
	// Will be overrided in inheritors to their table name
	protected tableName = '';

	constructor() {
		this.connection = connection;
	}

	// Searches instance in DB with given id, if does not exist - returns null
	async findOneById(id: number): Promise<Type | null> {
		const sql = `SELECT * FROM "${this.tableName}" WHERE id = $1`;
		const source = await this.connection.fetchRow(sql, [id]);
		if (!source) {
			return null;
		}
		return <Type>(<unknown>source);
	}

	// Gets all the rows of current "this.tableName" in DB
	// filter is a key-value pairs object, where key is column name and value - value for this column
	async findAll(
		filter: object = {},
		orderBy = 'id',
		limit = 'ALL',
		offset = 0,
	): Promise<Array<Type>> {
		let sql = `SELECT * FROM "${this.tableName}"`;
		if (Object.keys(filter).length != 0) {
			const custom_filter = Object.keys(filter)
				.map((key) => {
					if (typeof filter[key] == 'string') {
						return `${key} = '${filter[key]}' `;
					} else {
						return `${key} = ${filter[key]} `;
					}
				})
				.join('AND ');
			sql += ' WHERE ' + custom_filter;
		}
		sql += `ORDER BY ${orderBy} LIMIT ${limit} OFFSET ${offset}`;
		const sources = await this.connection.fetchRows(sql, []);
		return sources.map((source) => <Type>(<unknown>source));
	}

	async findAllWithLimit(filter: object = {}, limit: number): Promise<Array<Type>> {
		let sql = `SELECT * FROM "${this.tableName}"`;
		if (Object.keys(filter).length != 0) {
			const custom_filter = Object.keys(filter)
				.map((key) => {
					if (typeof filter[key] == 'string') {
						return ` ${key} = '${filter[key]}' `;
					} else {
						return ` ${key} = ${filter[key]} `;
					}
				})
				.join(' AND ');
			sql += ' WHERE ' + custom_filter;
		}
		sql += ' LIMIT $1';

		const sources = await this.connection.fetchRows(sql, [limit]);
		return sources.map((source) => <Type>(<unknown>source));
	}

	// Receives model that represents all the columns (except "id" and those, that have DEFAULT values)
	// of current entity for "this.tableName" table of DB
	// Example: table "city" with columns "id", "name"...
	// city: CityModel (actual interface for current table) = {name: "Moscow"...};
	// const createdRecord = await insert(city)
	async insert(model: Type): Promise<Type> {
		try {
			return <Type>(
				(<unknown>await this.connection.insert(this.tableName, <object>(<unknown>model)))
			);
		} catch (e) {
			throw new ServerErrorError(`Could not create ${this.tableName}.`);
		}
	}

	// Receives model that represents all the columns of current entity for "this.tableName" table of DB
	// In DB searches record with provided "id", than changes its values
	// Example: table "city" with columns "id", "name"...
	// city: CityModel (actual interface for current table) = {name: "Moscow"...};
	// const createdRecord = await insert(city)
	async update(model: Type): Promise<Type> {
		try {
			return <Type>(
				(<unknown>await this.connection.update(this.tableName, <object>(<unknown>model)))
			);
		} catch (e) {
			throw new ServerErrorError(`Could not update ${this.tableName}.`);
		}
	}

	async updateByParams(model: Type, filter: object): Promise<Type> {
		try {
			const columnValues = Object.values(<object>(<unknown>model));
			const sqlSets = Object.keys(<object>(<unknown>model))
				.map((columnName: string, i: number) => `"${columnName}" = $${++i}`)
				.join(', ');
			const paramsSets = Object.keys(filter)
				.map(
					(columnName: string, i: number) =>
						`"${columnName}" = $${++i + Object.keys(<object>(<unknown>model)).length}`,
				)
				.join(' AND ');

			const sql = `UPDATE "${this.tableName}" SET ${sqlSets} WHERE ${paramsSets} RETURNING *`;

			for (const paramValue of Object.values(filter)) {
				columnValues.push(paramValue);
			}

			const res = await this.connection.fetchRow(sql, columnValues);
			if (!res) {
				console.error(`Update error. Statement: ${sql}, params: ${columnValues.join(', ')}.`);
				throw new ServerErrorError('Update error.');
			}
			return <Type>(<unknown>res);
		} catch (e) {
			throw new ServerErrorError(`Could not update ${this.tableName}.`);
		}
	}

	// Deletes record by id in current "this.tableName" table and returns deleted record
	async delete(id: number): Promise<Type | null> {
		const sql = `DELETE FROM "${this.tableName}" WHERE id = $1 RETURNING *`;
		const source = await this.connection.fetchRow(sql, [id]);
		if (!source) {
			return null;
		}

		return <Type>(<unknown>source);
	}

	// Deletes record by params in current "this.tableName" table and returns deleted record
	async deleteByParams(columnToValue: object): Promise<Type | null> {
		const columnKeys = Object.keys(columnToValue);
		const values = Object.values(columnToValue);
		const sqlSets = columnKeys
			.map((columnName: string, index: number) => `"${columnName}" = $${++index}`)
			.join(' AND ');
		const sql = `DELETE FROM "${this.tableName}" WHERE ${sqlSets} RETURNING *`;
		const source = await this.connection.fetchRow(sql, values);
		if (!source) {
			return null;
		}

		return <Type>(<unknown>source);
	}

	// Method  was made for checking if record with current params exists and returns it
	// May have other uses..
	// Receives model that represents all the columns (except "id") of current entity for "this.tableName" table of DB
	async findOneByParams(columnToValue: object): Promise<Type | null> {
		const columnKeys = Object.keys(columnToValue);
		const values = Object.values(columnToValue);
		const sqlSets = columnKeys
			.map((columnName: string, index: number) => `"${columnName}" = $${++index}`)
			.join(' AND ');

		const sql = `SELECT * FROM "${this.tableName}" WHERE ${sqlSets} LIMIT 1`;
		const source = <Type>(<unknown>await this.connection.fetchRow(sql, values));

		return <Type>(<unknown>source);
	}

	async findOneByParam(columnName: string, columnValue: string): Promise<Type | null> {
		const sql = `SELECT * FROM "${this.tableName}" WHERE $1 = $2 LIMIT 1`;
		const source = await this.connection.fetchRow(sql, [columnName, columnValue]);
		if (!source) {
			return null;
		}
		return <Type>(<unknown>source);
	}

	// Checks if there is already record with such data, if no - creates it
	// Receives model that represents all the columns (except "id") of current entity for "this.tableName" table of DB
	async findOneByParamsOrCreate(columnToValue: object): Promise<Type> {
		let source = await this.findOneByParams(columnToValue);

		if (!source) {
			source = await this.insert(<Type>(<unknown>columnToValue));
		}
		return <Type>(<unknown>source);
	}
}

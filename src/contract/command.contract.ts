import { Request, Response } from 'express';
import { UnauthorizedError } from '../error/unauthorized.error';
import { userStorage } from '../module/user/storage/user.storage';
import { ErrorResponse } from '../response/error.response';
import { SuccessResponse } from '../response/success.response';
import { SwaggerService } from '../swagger/services/swagger.service';

export abstract class CommandContract {
	protected onlyAuthorized = false;
	protected endpointDescription = 'Description is not provided';
	protected forbiddenComment = '';
	protected notFoundComment = '';
	protected serverErrorComment = '';
	protected unprocessableEntityComment = '';
	protected fileNames: string[] = [];

	/**
	 * Handles request and sends response
	 * @param { Request } req
	 */
	abstract run(req: Request): Promise<any>;

	async runExternal(req: Request, res: Response): Promise<void> {
		try {
			if (this.onlyAuthorized && !userStorage.get()) {
				throw new UnauthorizedError('User is unauthorized.');
			}
			const response = await this.run(req);
			res.send(new SuccessResponse(response));
		} catch (e) {
			if (
				(<{ code: number }>e).code > 500 ||
				(<{ code: number }>e).code < 200 ||
				typeof (<{ code: number }>e).code != 'number'
			) {
				(<{ code: number }>e).code = 500;
			}
			res.status((<{ code: number }>e).code);
			res.send(new ErrorResponse(e));
		}
	}

	isAuthRequired(): boolean {
		return this.onlyAuthorized;
	}
	getDocs(): any {
		const docs = new SwaggerService(this.endpointDescription);
		if (this.fileNames.length != 0)
			for (const fileName of this.fileNames) docs.addFileReq(fileName);
		if (this.forbiddenComment != '') docs.useForbiddenResponse(this.forbiddenComment);
		if (this.notFoundComment != '') docs.useNotFoundResponse(this.notFoundComment);
		if (this.serverErrorComment != '') docs.useServerErrorResponse(this.serverErrorComment);
		if (this.unprocessableEntityComment != '')
			docs.useUnprocessableEntityResponse(this.unprocessableEntityComment);
		return docs;
	}
}

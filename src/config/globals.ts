const APP_ROOT = __dirname + '/../..';

export const globals = {
	APP_PORT: process.env.PORT || 5001,
	APP_ROOT,
	PUBLIC_ROOT: APP_ROOT + '/public',
	BASE_PATH: process.env.BASE_PATH || '',
	frontend_settings: {
		FRONTEND_PATH: process.env.FRONTEND_PATH || 'http://localhost:3001',
		PASSWORD_RESET_PATH: process.env.PASSWORD_RESET_PATH || '/auth/reset-password',
		EMAIL_CONFIRM_PATH: process.env.EMAIL_CONFIRM_PATH || '/auth/confirm-email',
	},
};

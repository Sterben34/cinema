export const db = {
	database: process.env.DB_NAME || 'movie',
	user: process.env.DB_USER || 'postgres',
	password: process.env.DB_PASSWORD || 'pass',
	host: process.env.DB_HOST || '127.0.0.1',
	port: Number(process.env.DB_PORT) || 5432,
};

import { ModuleContract } from '../contract/module.contract';
import { Type } from '../contract/type.contract';
import { AuthModule } from '../module/auth/auth.module';
import { MovieModule } from '../module/movie/movie.module';
import { UserModule } from '../module/user/user.module';
const moduleClasses: Array<Type<ModuleContract>> = [UserModule, AuthModule, MovieModule];

export const modules = moduleClasses.map((moduleClass) => new moduleClass());

import express, { Express, Request, Response } from 'express';
import { Server } from 'http';
// Routes
import { router } from './app-routing';
// Settings
import { globals } from './config/globals';
// Middlewares
import bodyParser from 'body-parser';
import cors from 'cors';
import fileUpload from 'express-fileupload';
import { AuthorizeUserInterceptor } from './module/user/interceptor/authorize-user.interceptor';
// Swagger stuff
import * as swaggerUi from 'swagger-ui-express';
import { swaggerDocument } from './swagger/swagger';
import { SwaggerUiOptions } from 'swagger-ui-express';

export class App {
	app: Express;
	server!: Server;
	port: number;

	constructor() {
		this.app = express();
		this.port = Number(globals.APP_PORT);
	}

	useMiddleware(): void {
		this.app.use(bodyParser.json());
		this.app.use(bodyParser.urlencoded({ extended: true }));
		this.app.use(
			fileUpload({
				createParentPath: true,
			}),
		);
		this.app.use(cors());

		const authorizeUserInterceptor = new AuthorizeUserInterceptor();
		this.app.use(authorizeUserInterceptor.intercept.bind(authorizeUserInterceptor));
	}

	useRoutes(): void {
		this.app.use(router);
		this.app.use((req: Request, res: Response) => {
			res.status(404);
			res.json({ error: 'Not found' });
		});
	}

	baseInit(): void {
		this.app.use(globals.BASE_PATH, express.static(globals.PUBLIC_ROOT));
	}

	useSwagger(): void {
		const options: SwaggerUiOptions = {
			swaggerOptions: {
				defaultModelsExpandDepth: -1,
				docExpansion: null,
			},
		};
		this.app.use('/docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument, options));
	}

	public async init(): Promise<void> {
		this.useSwagger();
		this.useMiddleware();
		this.useRoutes();
		this.baseInit();
		this.server = this.app.listen(this.port);
		console.log(`Server is running on port:${this.port}`);
	}

	public close(): void {
		this.server.close();
	}
}

import { SwaggerParameter } from './swagger-parameter';

export class SwaggerCallParameter {
	constructor(
		private where: string, // 'query' | 'path' | 'header' | 'cookie'
		private name: string, // The name of param, if in 'path' - must be the same as there!
		private type: string | SwaggerParameter, // Type of param
		// private examples: any[],
		private required = true,
		private description = 'No description', // Some description if needed
	) {}

	get(): any {
		return {
			in: this.where,
			name: this.name,
			schema:
				typeof this.type == 'string'
					? {
							type: this.type,
					  }
					: this.type.get(),
			required: this.required,
			description: this.description,
			// examples: this.examples.map((example, index) => {
			// 	return {
			// 		[index]: { value: example },
			// 	};
			// }),
		};
	}
}

import { SwaggerParameter } from './swagger-parameter';

export class SwaggerResponce {
	constructor(
		private status: number,
		private description: string,
		private params: SwaggerParameter[],
		private componentName = '',
	) {}

	addDiffetentContentType(): SwaggerResponce {
		return this;
	}

	get(): object {
		const props = {};
		for (const param of this.params) {
			props[param.get().name] = {
				type: param.get().type,
			};
		}

		return {
			[this.status]: {
				description: this.description,
				content: {
					'application/json': {
						schema:
							this.componentName.length == 0
								? {
										type: 'object',
										properties: props,
								  }
								: {
										$ref: `#/components/schemas/${this.componentName}`,
								  },
					},
				},
			},
		};
	}
}

import { CustomBodyParam } from './custom-body-param';

export class SwaggerParameter {
	constructor(private name: string, private type: string) {}

	get(): CustomBodyParam {
		return {
			name: this.name,
			type: this.type,
		};
	}
}

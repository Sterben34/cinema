import { ParseDtoService } from './services/parse-dto.service';
import { PathsBuilderService } from './services/paths-builder.service';

export const swaggerDocument = {
	openapi: '3.0.1',
	info: {
		version: '1.0.0',
		title: 'APIs Document',
		description: 'API documentation for Education project',
		termsOfService: '',
		contact: {
			name: 'Dmitry Kouzma',
			email: 'kouzma.d@gmail.com',
			url: '',
		},
		// license: {
		// 	name: 'Apache 2.0',
		// 	url: 'https://www.apache.org/licenses/LICENSE-2.0.html',
		// },
	},
	servers: [
		{
			url: 'http://localhost:5001',
			description: 'Local server',
		},
		// {
		// 	url: 'https://app-dev.herokuapp.com/api/v1',
		// 	description: 'DEV Env',
		// },
	],
	paths: new PathsBuilderService().getPaths(),
	components: {
		schemas: ParseDtoService.getDtoComponents(),
	},
};

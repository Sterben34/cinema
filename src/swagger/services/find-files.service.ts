import path from 'path';
import fs from 'fs';

export class FindFilesService {
	static getPathsToFiles(parentDirectiory: string, filterRegex: RegExp): string[] {
		const directoryChildren = fs.readdirSync(parentDirectiory);
		const files: string[] = [];

		for (const child of directoryChildren) {
			const filename = path.join(parentDirectiory, child);
			const stat = fs.lstatSync(filename);

			if (stat.isDirectory()) {
				files.push(...this.getPathsToFiles(filename, filterRegex)); //recurse
			} else if (filterRegex.test(filename)) {
				files.push(path.join(parentDirectiory, child));
			}
		}
		return files;
	}
}

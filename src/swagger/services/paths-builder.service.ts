import { modules } from '../../config/modules';
import { CommandContract } from '../../contract/command.contract';
import { ModuleContract } from '../../contract/module.contract';
import { Route } from '../../contract/route.contract';
import { GetParamsFromUrlHelper } from '../helpers/get-params-from-url.helper';
import { SwaggerCallParameter } from '../swagger-datatypes/swagger-call-parameter';
import { ParseDtoService } from './parse-dto.service';

export class PathsBuilderService {
	private availableComponents: string[];
	private command: CommandContract | undefined;

	constructor() {
		this.availableComponents = Object.keys(ParseDtoService.getDtoComponents());
	}

	getPaths(): object {
		const paths = modules.map((module: ModuleContract) => {
			const basePath = module.basePath;
			const routesInModule = module.routes.map((route: Route) => {
				const path = basePath + route.path;
				const method = route.method.toLocaleLowerCase();
				this.command = new route.command();

				let commandName = this.command.constructor.name;
				if (commandName.endsWith('Command')) {
					commandName = commandName.slice(0, commandName.length - 7);
				}
				const reqDto = commandName.concat('Dto');
				const resDto = commandName.concat('ResponseDto');

				const swagger = this.command.getDocs() ? this.command.getDocs() : undefined;
				if (!swagger) {
					return;
				}

				if (this.availableComponents.indexOf(reqDto) != -1) swagger.addJSONReqBody(reqDto);
				if (this.availableComponents.indexOf(resDto) != -1) swagger.addSuccessResponse(resDto);

				if (this.command.isAuthRequired()) {
					swagger.addCallParams([
						new SwaggerCallParameter('headers', 'token', 'string', true, 'User auth token'),
					]);
					swagger.useUnauthorizedResponse();
				}

				const formatedPath = this.formatPath(path);
				swagger.addCallParams(GetParamsFromUrlHelper.getPathParams(path));

				const docs = swagger.getDocs();
				docs.tags = [formatedPath.split('/')[1]];
				return {
					[formatedPath]: {
						[method]: docs,
					},
				};
			});

			return routesInModule;
		});
		const nonEmptyPaths = this.handleSamePathsWithDifferentMethods(paths);
		return nonEmptyPaths;
	}

	formatPath(path: string): string {
		let formatedPath = '';
		for (const part of path.split('/')) {
			if (part && part[0] == ':') {
				formatedPath += `/{${part.slice(1)}}`;
			} else {
				formatedPath += `/${part}`;
			}
		}
		return formatedPath.slice(1);
	}

	handleSamePathsWithDifferentMethods(
		paths: (
			| {
					[x: string]: {
						[x: string]: any;
					};
			  }
			| undefined
		)[][],
	): object {
		const nonEmptyPaths = {};
		const usedPaths: string[] = [];
		for (const path of paths) {
			for (const pathObj of path) {
				if (pathObj) {
					const pathStr = Object.keys(pathObj)[0];

					if (usedPaths.indexOf(pathStr) == -1) {
						nonEmptyPaths[pathStr] = pathObj[pathStr];
						usedPaths.push(pathStr);
					} else {
						const newMethod = Object.keys(<object>Object.values(pathObj)[0])[0];

						nonEmptyPaths[pathStr][newMethod] = pathObj[pathStr][newMethod];
					}
				}
			}
		}
		return nonEmptyPaths;
	}
}

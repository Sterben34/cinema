import fs from 'fs';
import { globals } from '../../config/globals';
import { FindFilesService } from './find-files.service';
import { commandsToPathParams } from './get-commands-paths.service';

export class ParseDtoService {
	static getDtoComponents(): any {
		const pathsToDto = FindFilesService.getPathsToFiles(globals.APP_ROOT, /\.dto\.ts/);
		const ComponnetsObj = {};
		const extended = {};

		for (const path of pathsToDto) {
			const text = fs.readFileSync(path).toString();

			const interfaces = this.getInterfaces(text);

			for (const key of Object.keys(interfaces)) {
				if (typeof interfaces[key] == 'string') {
					extended[key] = interfaces[key];
					continue;
				}
				ComponnetsObj[key] = interfaces[key];
			}
		}

		for (const key of Object.keys(extended)) {
			ComponnetsObj[key] = ComponnetsObj[extended[key]];
		}

		return ComponnetsObj;
	}

	static getInterfaces(fileText: string): object {
		const result = {};
		const intefaces = fileText.split('interface').slice(1);
		for (const inteface of intefaces) {
			const name = inteface.trim().split(' ')[0];
			if (inteface.trim().split(' ')[1] == 'extends') {
				result[name] = inteface.trim().split(' ')[2];
				continue;
			}
			const startIndex = inteface.indexOf('{') + 3;
			const endIndex = inteface.indexOf('}') - 1;

			result[name] = this.getInterfaceObject(
				inteface.slice(startIndex, endIndex),
				name.slice(
					0,
					name.indexOf('Response') != -1 ? name.indexOf('Response') : name.indexOf('Dto'),
				),
			);
		}
		return result;
	}

	static getInterfaceObject(interfaceStr: string, commandName: string): object {
		const interfaceObj = {
			type: 'object',
			required: [] as string[],
			properties: {},
		};
		const rows = interfaceStr.split('\n');
		for (const row of rows) {
			let [name, type] = row.trim().slice(0, -1).split(':');
			if (commandsToPathParams[commandName])
				if (!type || !name || commandsToPathParams[commandName].indexOf(name) != -1) continue;
			if (!type) continue;
			interfaceObj.required.push(name);

			if (name.indexOf('?') != -1) {
				interfaceObj.required.pop();
				name = name.slice(0, -1);
			}
			type = type.trim();

			interfaceObj.properties[name] = this.BuildSwaggerObjectFromType(type);
		}

		return interfaceObj;
	}
	static BuildSwaggerObjectFromType(type: string): object {
		const COMMON_DATA_TYPES = ['boolean', 'string', 'number'];
		const actualDataType = {};

		if (type.includes('[]')) {
			type = type.substring(0, type.length - 2);
			actualDataType['type'] = 'array';
			actualDataType['items'] = this.BuildSwaggerObjectFromType(type);
			return actualDataType;
		}
		if (COMMON_DATA_TYPES.indexOf(type) == -1) {
			actualDataType['$ref'] = `#/components/schemas/${type}`;
			return actualDataType;
		}
		if (type == 'number') type = 'integer';
		actualDataType['type'] = type;
		return actualDataType;
	}
}

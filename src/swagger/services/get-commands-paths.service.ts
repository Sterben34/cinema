import { modules } from '../../config/modules';
import { ModuleContract } from '../../contract/module.contract';
import { Route } from '../../contract/route.contract';
import { GetParamsFromUrlHelper } from '../helpers/get-params-from-url.helper';

class GetCommandsPathsService {
	static getCommandsToPaths(): object {
		const result = {};
		const paths = modules.map((module: ModuleContract) => {
			const basePath = module.basePath;
			const routesInModule = module.routes.map((route: Route) => {
				const path = basePath + route.path;
				const method = route.method.toLocaleLowerCase();
				const command = new route.command();

				let commandName = command.constructor.name;
				if (commandName.endsWith('Command')) {
					commandName = commandName.slice(0, commandName.length - 7);
				}
				result[commandName] = [];
				for (const param of GetParamsFromUrlHelper.getPathParams(path)) {
					result[commandName].push(param.get().name);
				}
			});
		});
		return result;
	}
}

export const commandsToPathParams: any = GetCommandsPathsService.getCommandsToPaths();

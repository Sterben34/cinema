// TODO add examples into SwaggerCallParameter(for now they are commented)
import { CustomBodyParam } from '../swagger-datatypes/custom-body-param';
import { SwaggerCallParameter } from '../swagger-datatypes/swagger-call-parameter';
import { SwaggerParameter } from '../swagger-datatypes/swagger-parameter';
import { SwaggerResponce } from '../swagger-datatypes/swagger-responce';

export class SwaggerService {
	private callParams: SwaggerCallParameter[] = [];
	private responses: SwaggerResponce[] = [];
	private reqBody: object = {
		description: 'Json body of request',
		required: true,
		content: {},
	};

	constructor(
		private summary: string, // Description of the endpoint
	) {}

	addCallParams(params: SwaggerCallParameter[]): SwaggerService {
		this.callParams.push(...params);
		return this;
	}
	addFileReq(fileName: string): SwaggerService {
		if (!this.reqBody['content']['multipart/form-data']) {
			this.reqBody['description'] = 'Data and files of body';
			this.reqBody['content']['multipart/form-data'] = {
				schema: {
					type: 'object',
					properties: {
						[fileName]: {
							type: 'string',
							format: 'binary',
						},
					},
				},
			};
		} else
			this.reqBody['content']['multipart/form-data']['schema']['properties'][fileName] = {
				type: 'string',
				format: 'binary',
			};
		return this;
	}
	// Uses prebuilded component from ./components/* or custom variant
	addJSONReqBody(dtoName = '', custom: CustomBodyParam[] = []): SwaggerService {
		if (dtoName) {
			this.reqBody['content']['application/json'] = {
				schema: {
					$ref: `#/components/schemas/${dtoName}`,
				},
			};
			return this;
		}

		const props = {};
		for (const param of custom) {
			props[param.name] = {
				type: param.type,
			};
		}

		this.reqBody['content']['application/json'] = {
			schema: {
				type: 'object',
				properties: props,
			},
		};

		return this;
	}

	addSuccessResponse(componentName: string): SwaggerService {
		this.responses.push(new SwaggerResponce(200, 'Successfull.', [], componentName));
		return this;
	}

	useForbiddenResponse(message = 'Forbidden response.'): SwaggerService {
		this.responses.push(
			new SwaggerResponce(403, message, [
				new SwaggerParameter('status', 'integer'),
				new SwaggerParameter('message', 'string'),
			]),
		);
		return this;
	}

	useNotFoundResponse(message = 'Not found.'): SwaggerService {
		this.responses.push(
			new SwaggerResponce(404, message, [
				new SwaggerParameter('status', 'integer'),
				new SwaggerParameter('message', 'string'),
			]),
		);
		return this;
	}

	useServerErrorResponse(message = 'Server error.'): SwaggerService {
		this.responses.push(
			new SwaggerResponce(500, message, [
				new SwaggerParameter('status', 'integer'),
				new SwaggerParameter('message', 'string'),
			]),
		);
		return this;
	}

	useUnauthorizedResponse(message = 'User is not authorised.'): SwaggerService {
		this.responses.push(
			new SwaggerResponce(401, message, [
				new SwaggerParameter('status', 'integer'),
				new SwaggerParameter('message', 'string'),
			]),
		);
		return this;
	}

	useUnprocessableEntityResponse(message = 'Unprocessable entity.'): SwaggerService {
		this.responses.push(
			new SwaggerResponce(422, message, [
				new SwaggerParameter('status', 'integer'),
				new SwaggerParameter('message', 'string'),
			]),
		);
		return this;
	}

	getDocs(): object {
		const responses = {};
		for (const res of this.responses.map((res) => res.get())) {
			responses[Object.keys(res)[0]] = res[Object.keys(res)[0]];
		}

		const docs = {
			summary: this.summary,
			responses: responses,
		};
		// Do not add params to display, if not required any
		const callParams = this.callParams.map((param) => param.get());
		if (callParams.length != 0) {
			docs['parameters'] = callParams;
		}

		// Do not add requestBody to display, if not required any
		if (Object.keys(this.reqBody['content']).length != 0) {
			docs['requestBody'] = this.reqBody;
		}
		return docs;
	}
}

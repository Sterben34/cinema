import { SwaggerCallParameter } from '../swagger-datatypes/swagger-call-parameter';

export class GetParamsFromUrlHelper {
	static getPathParams(path: string): SwaggerCallParameter[] {
		const pathParams: SwaggerCallParameter[] = [];

		for (const part of path.split('/')) {
			if (part && part[0] == ':') {
				pathParams.push(new SwaggerCallParameter('path', part.slice(1), 'string'));
			}
		}
		return pathParams;
	}
}
